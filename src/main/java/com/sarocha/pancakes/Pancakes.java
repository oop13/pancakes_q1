/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.pancakes;


/**
 *
 * @author Sarocha
 */
public class Pancakes {

    private char feelings;

    public Pancakes() {
        askEmotions();
    }

    public void askEmotions() {
        System.out.println("Sherly's emotions today :");
    }

    public boolean topping(char mood) {
        switch (mood) {
            case 'H':
                System.out.println("I eat pancakes with blueberries.");
                break;
            case 'B':
                System.out.println("I eat pancakes with blueberries.");
                break;
            case 'U': 
                System.out.println("I eat pancakes with bacon and fried egg.");
                break;
            case 'S': 
                System.out.println("I eat pancakes with butter and honey.");
                break;
            case 'A': 
                System.out.println("I'm not hungry pancakes for today");
                break;
            default:
                System.out.println("I eat pancakes without topping");
                break;
        }
        return false;
    }
}
