/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.pancakes;

import java.util.Scanner;

/**
 *
 * @author Sarocha
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Pancakes pancake = new Pancakes();
        while (true) {
            char mood = inputMood(kb);
            if (mood == 'F') {
                System.out.println("I'm full!!!");
                break;
            }
            pancake.topping(mood);
        }
    }

    public static char inputMood(Scanner kb) {
        char mood = kb.next().charAt(0);
        return mood;
    }

}
